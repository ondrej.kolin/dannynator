/* Constants and vars for whole game */
var objects_label = undefined
var score_label = undefined
var multiplier = 0;
var spawn = false;
var rampage_sound;
var hp_remaining;
var music = undefined;
var bg_pool = []
var bg = undefined;
var bg_counter = 1;

const config = {
    type: Phaser.AUTO,
    width: 900,
    height: 800,
    scene: {
        preload: preload,
        create: create,
        update: update
    },
    parent: 'game',
    physics: {
        default: 'arcade',
        arcade: {},
        debug: true
    },
    chance: {
      threshold: 0.040,
      multiplier: 100,
      thresholds: [0.25, 0.50, 0.75, 1],
    },
    speed: {
      base: 200,
      distribution: 200,
    },
    rotation: {
      min: 0,
      max: 0.04
    },
    scale: {
      min: 0.8,
      max: 1.8,
      speed: {
        min: 0.0001,
        max: 0.0150
      }
    },
    font: {
      fontSize: 20,
      fontFamily: "Arial"
    },
    streak : {
      cooldown: 1.2,
      fontSize: 12,
      fontFamily: "Arial",
      fadingSpeed: 0.10,
      interval: 50,
      color: "red"
    },
    hp : 3,
    hp_face : {
      width: 40,
      height: 40
    },
    menu_header : {
      x: 100,
      y: 100,
      text: 'Dannynator',
      style: {
        fontSize: '64px',
        fontFamily: 'Arial',
        color: '#ffffff'
      },
    },
    hints: [
        "Hit faces with mouse",
        "Connect to streaks",
        "Donate Danny",
        "Missed face means hp loss"
    ],
    background_threshold: 30
};

var game = new Phaser.Game(config);
const pool = [];
var faces;

const FACE_SIZE = {
  "x" : 64,
  "y" : 64
};
/* Helper functions */

function randomPoint(p_threshold) {
  var threshold = p_threshold;
  if (threshold == undefined) {
    threshold = {"x":0, "y":0};
  }

  var x, y;
  if (Math.random() > 0.5) {
      x = Math.floor(Math.random()*2) * config.width;
      x += (x==0)*threshold.x
      y = Math.random()*config.height
  }
  else {
      x = Math.random()*config.width
      y = Math.floor(Math.random()*2) * config.height;
      y += (Number(y==0)*threshold.y)
  }
  return {"x": x, "y":y}
}

function angleToCenter(from, randomness) {
  centralPoint = new Phaser.Geom.Point(config.width/2,config.height/2);
  return Phaser.Math.Angle.BetweenPoints(from, centralPoint)*randomness/100
}

/* Phaser functions */

function preload ()
{
  this.load.audio("rampage", "sounds/rampage.mp3")
  this.load.audio("first_blood", "sounds/first_blood.mp3")
  this.load.audio("ancient", "sounds/ancient.mp3")
  this.load.audio("pepesong", "sounds/song.mp3")
  this.load.audio("gg", "sounds/gg.mp3")
  this.load.image("hp", "img/hp.png");
  this.load.image("hp_lost", "img/hp_lost.png");
  fetch("js/resources.json").then((response) => {
    if (response.status != 200) {
      console.log("Error loading data")
      return;
    }
    response.json().then( (data) => {
      data.images.forEach( (item) => {
        pool.push(item.name)
        this.load.image(item.name, data.path + item.file)
      });
      data.backgrounds.forEach((item) => {
        this.load.image(item, data.background_path + item)
        bg_pool.push(item)
      });
    })
  })
}

function addObject() {
  if (!spawn) {
    return;
  }
  debug()
  const image = pool[Math.floor(Math.random()*pool.length)];
  const where = randomPoint(FACE_SIZE)
  face = faces.create(where.x, where.y, image);
  face.setInteractive()
  face.on('pointerdown', function (pointer) {
    destroyObject(this, spawn)
  });
  angle = angleToCenter(face, 120);
  face.body.velocity.x = Math.cos(angle)*(config.speed.base + Math.floor(Math.random()*(config.speed.distribution)))
  face.body.velocity.y = Math.sin(angle)*(config.speed.base + Math.floor(Math.random()*(config.speed.distribution)))
  face.rotation_speed = Phaser.Math.FloatBetween(config.rotation.min, config.rotation.max)
  face.scaleX_speed = Phaser.Math.FloatBetween(config.scale.speed.min, config.scale.speed.max)
  face.scaleY_speed = Phaser.Math.FloatBetween(config.scale.speed.min, config.scale.speed.max)
}

function destroyObject(gameObject, count=true) {
  if (count) {
    multiplier+=1;
    setTimeout(function(){
      multiplier -= 1;
    }, config.streak.cooldown * 1000);
    score += multiplier*1
    if (multiplier >= 5) {
      if (rampage_sound == undefined) {
        rampage_sound = game.sound.add("rampage");
      }
      rampage_sound.play();
    }
    var label_cfg = {}
    label_cfg.fontFamily = config.streak.fontFamily;
    label_cfg.fontSize = (config.streak.fontSize * multiplier)
    label_cfg.color = config.streak.color
    var multiplier_label = game.object.add.text(gameObject.x, gameObject.y, multiplier + "x", label_cfg);
    multiplier_label.interval = setInterval(function () {
      if (multiplier_label.alpha <= 0) {
        multiplier_label.destroy();
        clearInterval(multiplier_label.interval)
        return;
      }
      multiplier_label.alpha -= config.streak.fadingSpeed;
    }, config.streak.interval);
  }
  updateScore();
  gameObject.destroy();
  debug();
}

function start() {
  menu_bg.setVisible(false);
  menu_name.setVisible(false);
  menu_play.setVisible(false);
  menu_score.setVisible(false);
  for (let i = 0; i<hints.length; ++i) {
    hints[i].setVisible(false);
  }
  spawn = true;
  score = 0
  hp = []
  hp_remaining = config.hp;
  for (let i=0; i<config.hp; ++i) {
    x = game.object.add.sprite(config.width - (i+0.5)* config.hp_face.width, config.hp_face.width/2, "hp")
    x.setDisplaySize(config.hp_face.width, config.hp_face.height)
    x.depth = 10
    hp.push(x)
  }
  updateScore();
}
var menu_bg;
var menu_name;
var menu_score;
var menu_play;
var hints = [];

function displayMenu() {
  menu_bg.setVisible(true)
  menu_name.setVisible(true)
  if (score != 0) {
    menu_score.setText("GG! Your score was: " + score);
    menu_score.setVisible(true)
  }
  menu_play.setVisible(true)
  for (let i = 0; i<hints.length; ++i) {
    hints[i].setVisible(true);
  }
}

function buildMenu() {
  menu_bg = game.object.add.rectangle(0,0,1800,1600, "0x33AAee");
  menu_bg.depth = 20;
  menu_bg.setVisible(false)

  menu_name = game.object.make.text(config.menu_header)
  menu_name.setOrigin(0.5);
  menu_name.setPosition(config.width/2, 300);
  menu_name.depth = 30;
  menu_name.setVisible(false)

  menu_score = game.object.make.text(config.font)
  menu_score.depth = 30;
  menu_score.setVisible(false)
  menu_score.setOrigin(0.5);
  menu_score.setFontSize(+(menu_score.style.fontSize.split("p")[0])*3 + "px")
  menu_score.setPosition(config.width/2, 400);

  menu_play = game.object.make.text(config.font)
  menu_play.setText(">> Play a new round! <<")
  menu_play.setPadding(10, 15)
  setInterval(() => {menu_play.setColor(menu_play.style.color == "black" ? "white" : "black")}, 300)
  menu_play.setBackgroundColor("#00FF00")
  menu_play.setInteractive()
  menu_play.setOrigin(0.5)
  menu_play.depth = 30;
  menu_play.setVisible(false)
  menu_play.setPosition(config.width/2, 500);
  menu_play.on('pointerdown', function (pointer) {
    start()
  });
  for (var i = 0; i<config.hints.length; ++i) {
    var hint = game.object.add.text(0, 0, config.hints[i], config.font);
    hint.setPosition(
      Phaser.Math.FloatBetween(0, config.width - hint.width),
      Phaser.Math.FloatBetween(0, config.height - hint.height)
    )
    hint.depth = 25;
    hints.push(hint)
  }

}

function randomBg() {
  texture = bg_pool[Phaser.Math.Between(0, bg_pool.length)]
  if (bg.visible == false) bg.setVisible(true)
  bg.setTexture(texture)
  bg.setOrigin(0)
  bg.setDisplaySize(config.width, config.height)
}

function create ()
{
  game.object = this;
  bg = game.object.add.image(0,0,"fake");
  music = game.sound.add("pepesong")
  music.loop = true
  music.play()
  score = 0;
  faces = this.physics.add.group()
  buildMenu()
  displayMenu()
}
/* UPDATE FUNCTIONS HELPERS */
function update_scale(item) {
  if ( item.scaleX > config.scale.max || item.scaleX < config.scale.min) {
        item.scaleX_speed = (item.scaleX_speed > 0 ? -1 : 1) * Phaser.Math.FloatBetween(config.scale.speed.min, config.scale.speed.max)
  }
  if ( item.scaleY > config.scale.max || item.scaleY < config.scale.min) {
    item.scaleY_speed = (item.scaleY_speed > 0 ? -1 : 1) * Phaser.Math.FloatBetween(config.scale.speed.min, config.scale.speed.max)
  }
  item.scaleX += item.scaleX_speed
  item.scaleY += item.scaleY_speed

}

function debug() {
  if ( objects_label == undefined) {
    objects_label = game.object.add.text(config.height-config.font.fontSize, 0, "Objects: {}")
  }
  objects_label.text = "Objects: " + faces.children.entries.length
}

function updateScore() {
  if ( score_label == undefined) {
    score_label = game.object.add.text(0, 0, "Score: {}", config.font)
    score_label.depth = 10;
  }
  console.log(score / config.background_threshold)
  if (parseInt(score / config.background_threshold) > bg_counter) {
    randomBg()
    bg_counter++;
  }
  score_label.text = "Score: " + score
}

function damage() {
  if (hp_remaining == -1) {
    return;
  }
  if (hp_remaining == config.hp ) {
    game.sound.add("first_blood").play()
  }
  else {
    if (hp_remaining == 0) {
      gameOver()
    } else {
      game.sound.add("ancient").play()
    }
  }
  hp_remaining--;
  if (hp_remaining >= 0) {
    hp[hp_remaining].setTexture("hp_lost")
  }
  console.log(hp_remaining)
}

function gameOver() {
  game.sound.add("gg").play()
  spawn = false;
  for (var i = 0; i<hp.length; ++i) {
    hp[i].destroy();
  }
  delete hp;
  while (faces.children.entries.length) {
    faces.children.entries.forEach((item) => { item.destroy() })
  }
  displayMenu();
}

/* UPDATE */
function update ()
{
  faces.children.entries.forEach((item)=>{
    item.rotation += item.rotation_speed
    update_scale(item)
    if (Phaser.Geom.Rectangle.Overlaps(game.object.physics.world.bounds, item.getBounds()) == false) {
      console.log(item.x < 300 ? "LEFT" : "RIGHT", item.y > 300 ? "TOP" : "BOTTOM", "X:" + item.x, "Y:" + item.y )
      console.log(item)
      damage()
      debug()
      item.destroy();
    }
  });
  if (Math.random() < config.chance.threshold * config.chance.multiplier) {
    const rnd = Math.random();
    var ogre = 1;
    for (let i = 0; i < config.chance.thresholds; ++i) {
      if (rnd < config.chance.thresholds[i]  ) {
        ogre++;
      }
      else {
        break;
      }
    }
    for (let i=0; i<ogre; ++i) {
      addObject();
    }
    config.chance.multiplier = 1;
  }
  else {
    config.chance.multiplier += 0.01;
  }
}
